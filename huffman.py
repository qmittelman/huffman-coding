#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: quentinmittelman
"""

import heapq
import pickle


def save(data, name):
    pFile = open(name, 'wb')
    pickle.dump(data, pFile)
    pFile.close()
    
def load(name):
    pFile = open(name, 'rb')
    pickle.load(pFile)
    pFile.close()


def incr_nested_dict(d, s):
    try:
        if len(s) == 1:
            d[s] += 1 
        else:
            incr_nested_dict(d[s[0]], s[1:])
    except:
        if len(s) == 1:
            d[s] = 1
        else:
            d[s[0]] = dict()
            incr_nested_dict(d[s[0]], s[1:])


def frequency(text, order):
    d_freq = dict()
    pattern = text[0:order - 1]
    for c in text[order-1:]:
        pattern += c
        incr_nested_dict(d_freq, pattern)
        pattern = pattern[1:]
    def norm_nested_dict(d, order):
        if order == 1:
            sum_value = sum(d.values())
            for key in d:
                d[key] = d[key]/sum_value
        else:
            for key in d:
                norm_nested_dict(d[key], order -1)
    norm_nested_dict(d_freq, order)
    return d_freq
        


def huffman_coding(table_freq):
    """ dict{char : float} -> dict{char : str}
    return the dictionnary of the codes given the probability of each character """
    ret = [(occ, [[c, '']]) for c, occ in table_freq.items()]
    heapq.heapify(ret)

    if len(ret) == 1:
        freq, llst = heapq.heappop(ret)
        llst[0][1] += '1'
        heapq.heappush(ret, (freq, llst))

    while len(ret) >= 2:
        tmp = []

        freq1, llst1 = heapq.heappop(ret)
        freq2, llst2 = heapq.heappop(ret)

        for el in llst1:
            el[1] = '0' + el[1]
            tmp.append(el)

        for el in llst2:
            el[1] = '1' + el[1]
            tmp.append(el)

        heapq.heappush(ret, (freq1 + freq2, tmp))

    return dict(ret[0][1])



def construct_coding(sample_text, order, name_of_table):
    #sample_text is the text used to determine the frequency of each character
    
    table_freq = frequency(sample_text, order)
    table_code = dict()
    
    def iter_nested_dict(d1, d2, order):
        if order == 1:
            d1 = huffman_coding(d2)
        else:
            for key in d2:
                d1[key] = dict()
                iter_nested_dict(d1[key], d2[key], order - 1)
    
    iter_nested_dict(table_code, table_freq, order)
    
    save(table_code, name_of_table)
    

def compress(file, order, name_of_table):
    table_code = load()
    pFile = open(file, 'rb')
    text = str(pFile.read())
    pFile.close()
    ext = ".hf" + order
    pComp = open(file + ext, 'wb')
    for i in range(order - 1):
        pComp.write(bin(ord(text[i])))
    def nested_dict(d, s):
        if len(s) == 1:
            return d[s]
        else:
            return nested_dict(d[s[0]], s[1:])
    code = ""
    for i in range(len(text)-order):
        code += nested_dict(table_code, text[i: i+order])
    pFichier.write(int('1'+code, 2).to_bytes(math.ceil(len(code) / 8) + 1, 'big'))
